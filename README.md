Team Members: Lola and Sherise

Team work
1. Sherise: Dockerfile.mysql and docker-compose.yaml
2. Lola: Dockerfile.app and README.md

Created a bitbucket shared repository which was cloned to local repo using git clone https://LolaD@bitbucket.org/sheriselam/devassessment1.git

This gave access to the local repo and allowed creation of working file, adding files to staging area and pushing files from staging area to 
local repo. 

Lola worked on building the Dockerfile.app, using the command 'nano Dockerfile.app' and the Sherise worked on building the Dockerfile.mysql and
the docker-compose.yaml file. 

Following the file builds, the files; schema-data.sql, Dockerfile.app, index.php, Dockerfile.sql, we added files to stage using
add, commit, status, push to ensure entry updated from local repo to remote repo. 
 
We created two separate branches, to work on our separate pieces, and merged on bitbucket using 'pull request'. 

The code built an application that creates an image for a database and for the app, using the docker-compose we linked the database
and the app together, to communicate and deliver a website. This website allowed for a search functions for country, using the UK as an 
example. With this search function, we can find the individuals in the searched country including all details such as name, email
and age. 

With regard to the docker-compose command, we used docker-compose up --build to force build the images,which entailed
the location of the file, the entrypoint and the image tags that communicated from the database server to the apache application.  


